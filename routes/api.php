<?php 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/home/user/check', 'API\Auth\LoginController@UserExits');
Route::post('/home/user/initiate', 'API\Auth\RegisterController@userInitiateRegistration');
Route::post('/home/user/login', 'API\Auth\LoginController@userLogin');
Route::post('/home/user/register', 'API\Auth\RegisterController@userCompleteRegistration');

Route::match(array('GET','POST'),'service/create', 'API\ServiceAuth\ServiceController@service_create');
// Route::post('service/create', 'API\ServiceAuth\ServiceController@service_create');	


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::any('/{a?}/{b?}/{c?}/{d?}/{e?}/{f?}/{g?}/{h?}/{i?}/{j?}/{k?}', function (){
    return [
        'status' => 0,
        'message' => 'The API route you were trying to access does not exist. Please recheck route and try again.',
        'error' => '404 Not Found'
    ];
});
