<?php

namespace App\Http\Controllers\API\ServiceAuth;

use App\Models\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Utils\ValidationsUtil;
use Illuminate\Support\Facades\Validator;
use DB;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }
// id, service_type, service_count, price_for_small_car, price_for_big_car, service_days_in_a_month, service_days_in_a_weak, service_image, service_time1, service_time2, service_time3, service_time4, status, created_at, updated_at.
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function service_create(Request $request)
    {
        $rules = ['service_type'=>'required', 'service_count'=>'required', 'price_for_small_car'=>'required', 'price_for_big_car'=>'required', 'service_days_in_a_month'=>'required', 'service_days_in_a_weak'=>'required', 'service_image'=>'required', 'service_time1'=>'required', 'service_time2'=>'required', 'service_time3'=>'required', 'service_time4'=>'required', 'status'=>'required'];
        $validator = Validator::make($request->all(), $rules);
        if ($validator-> fails()){
             return [
                'status' => 0,
                'message' => 'One or more of your provided details failed validation. Please try again.',
                'success' => false,
                'data' => null,
                'fields' => $validator->errors()
            ];
        }else{
        $input = $request->all();
            return [
                'status' => 0,
                'message' => 'One or more of your provided details failed validation. Please try again.',
                'success' => true,
                'data' => $input,
            ];
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        //
    }
}
