<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\APIController;
use App\Models\OtpMapping;
use App\Models\PersonalAccessClient;
use App\Models\User;
use App\Utils\OtpUtils;
use App\Utils\ValidationsUtil;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends APIController
{



    public function findUserByMobile(string $mobile){
        return User::where('mobile', $mobile)->where('type', User::Roles['User'])->first();
    }

    public function userInitiateRegistration(Request $r)
    {
        $validationsUtil=new ValidationsUtil();
        $validationsUtil->setFields(['mobile']);
        if (!$validationsUtil->hasAllFields($r->all())) {
            return [
                'status' => 0,
                'message' => $validationsUtil->getValidationErrorString(),
                'found' => false
            ];
        }

        $validator = Validator::make($r->all(), ['mobile' => ['required', 'string', 'size:10']]);
        if ($validator->fails()) {
            return [
                'status' => 0,
                'message' => 'Your provided mobile number is not in valid format.',
                'found' => false
            ];
        }
        $user=null;
        if (!($user=$this->findUserByMobile($r->mobile)))
        {
            $otp=OtpUtils::generate();
            $mapping=OtpMapping::firstOrCreate(['mobile'=>$r->mobile]);
            $mapping->mobile = $r->mobile;
            $mapping->otp = $otp;
            $mapping->save();
            $result = OtpUtils::sendAppOtp($otp, $r->mobile);
            if (!$result) {
                return [
                    'status' => 0,
                    'message' => 'We encountered an error sending otp to that mobile number. Please try again in a while.',
                    'found' => false
                ];
            }
            return [
                'status' => 1,
                'message' => 'We have sent an otp at your mobile number.',
                'found' => false
            ];


        }else{
            return [
                'status' => 0,
                'message' => 'Your provided mobile number is already taken. Try logging in or use a different one.',
                'found' => true
            ];

        }




    }




    public function userCompleteRegistration(Request $r){
        $validationsUtil = new ValidationsUtil();
        $validationsUtil->setFields(['mobile', 'otp', 'name', 'email', 'password']);
        if (!$validationsUtil->hasAllFields($r->all())) {
            return [
                'status' => 0,
                'message' => $validationsUtil->getValidationErrorString(),
                'success' => false,
                'data' => null
            ];
        }

        $validator = Validator::make($r->all(), [
            'otp' => OtpUtils::getOTPValidationRule(),
            'name' => ['bail', 'required', 'string', 'min:2', 'max:50'],
            'mobile' => ['bail', 'required', 'digits:10'],
            'email' => ['bail', 'email', 'unique:users'],
            'password' => ['bail', 'required', 'string', 'min:4', 'max:64']
        ]);
        if ($validator->fails()) {
            return [
                'status' => 0,
                'message' => $validator->errors()->first(),
                'success' => false,
                'data' => null,
                'fields' => $validator->errors()
            ];
        }


        $entry = OtpMapping::where('mobile', $r->mobile)->where('otp',$r->otp)->first();
        if (!$entry) {
            return [
                'status' => 0,
                'message' => 'Your provided otp does not match with the one sent to your mobile number.',
                'success' => false,
                'data' => null,
            ];
        }
        $user = null;
        $user = User::where('mobile', $r->mobile)->first();
        if ($user) {
            return [
                'status' => 0,
                'message' => 'Your provided mobile number is already taken. Try logging in or use a different one.',
                'success' => false,
                'data' => null,
            ];
        }
        else {
            $user = new User();
            $user->name = $r->name;
            $user->email = $r->email;
            $user->password = Hash::make($r->password);
            $user->type = User::Roles['User'];
            $user->mobile = $r->mobile;
            $user->save();
            PersonalAccessClient::makeNewConditionally();
            $token = $user->createToken('Easycari')->accessToken;
            $data = [
                'id' => $user->id,
                'mobile' => $user->mobile,
                'name' => $user->name,
                'email' => $user->email,
                'type'=>$user->type,
                'token' => $token,
            ];
            return [
                'status' => 1,
                'message' => "You've been signed up successfully.",
                'success' => true,
                'data' => $data
            ];
        }
    }
}
