<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\APIController;
use App\Models\OtpMapping;
use App\Models\PersonalAccessClient;
use App\Utils\OtpUtils;
use App\Utils\ValidationsUtil;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\OauthAccessTokens;
use App\Models\User;


class LoginController extends APIController
{
    private $request;
    public function __construct(Request $request)
    {
        $this->request = $request;

    }
    public function findUserByMobile(string $mobile){
        return User::where('mobile', $mobile)->where('type', User::Roles['User'])->first();
    }
    public function UserExits(Request $r){
        $validationUtils=new ValidationsUtil();
        $validationUtils->setFields(['mobile']);
        if (!$validationUtils->hasAllFields($r->all()))
        {
            return [
              'status' =>0,
              'message'=>$validationUtils->getValidationErrorString(),
              'found' =>false
            ];
        }
        $validator=Validator::make($r->all(),['mobile' => ['required', 'string', 'size:10']]);
        if ($validator->fails())
        {
            return[
                'status'=>0,
                'message'=>'Your provided mobile number is not in valid format.',
                'found'=>false

            ];
        }
        $user=null;
        if (!($user=$this->findUserByMobile($r->mobile)))
        {
            return [
                'status' => 1,
                'message' => 'We could not find the user associated with that mobile number.',
                'found' => false
            ];
        }else{
            $otp=OtpUtils::generate();
            $user->otp=$otp;
            $user->save();
            $result=OtpUtils::sendAppOtp($otp,$r->mobile);
            if (!$result)
            {
                return [
                    'status' => 0,
                    'message' => 'We encountered an error sending otp to that mobile number. Please try again in a while.',
                    'found' => false
                ];
            }else{
                return [
                    'status' => 1,
                    'message' => 'We have sent an otp at your registered mobile number.',
                    'found' => true
                ];
            }

        }



    }
    public function userLogin(Request $r){
        $validationUtil = new ValidationsUtil();
        $validationUtil->setFields(['mobile', 'otp']);
        if (!$validationUtil->hasAllFields($r->all())) {
            return [
                'status' => 0,
                'message' => $validationUtil->getValidationErrorString(),
                'success' => false,
                'data' => null
            ];
        }

        $validator = Validator::make($r->all(), [
            'otp' => OtpUtils::getOTPValidationRule(),
            'mobile' => ['bail', 'required', 'digits:10'],
        ]);
        if ($validator->fails()) {
            return [
                'status' => 0,
                'message' => 'One or more of your provided details failed validation. Please try again.',
                'success' => false,
                'data' => null,
                'fields' => $validator->errors()
            ];
        }

        $user = null;
        if (!($user = $this->findUserByMobile($r->mobile))) {
            return [
                'status' => 0,
                'message' => 'We could not find the user associated with that mobile number.',
                'success' => false,
                'data' => null
            ];
        }
        else {
            $user = $this->findUserByMobile($r->mobile);
            if (OtpMapping::matches($user, $r->otp)) {
                PersonalAccessClient::makeNewConditionally();
                $token = $user->createToken('EasyCari')->accessToken;
                $data = [
                    'id' => $user->id,
                    'mobile' => $r->mobile,
                    'name' => $user->name,
                    'email' => $user->email,
                    'type'=>$user->type,
                    'token' => $token,
                ];
                return [
                    'status' => 1,
                    'message' => "You've been logged in successfully.",
                    'success' => true,
                    'data' => $data
                ];
            }
            else {
                return [
                    'status' => 0,
                    'message' => 'The otp you typed does not match with the one we sent on your registered mobile.',
                    'success' => false,
                    'data' => null
                ];
            }
        }
    }
}
